#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>


#include "util.h"

//will search an array of commands and return the correct index.
int getCmdIndex(char * cmd);

//for mkdir
void make_directory(char path[124]);
int enter_name(MINODE* mip, int myino, char* myname);
int my_mkdir(MINODE* pmip, char* child_name);

//for rmdir
int isEmpty(MINODE* mip);
void rmChild(MINODE* parent, char* name);
void remove_directory(char* path);

//for creat
void create_file(char path[124]);
int my_creat(MINODE* pmip, char* child_name);

//for rm

//for cd, ls, pwd
void print(MINODE* mip, char* name);
void print_directory(MINODE* mip);

void ls(char* pathname);
void cd(char* pathname);
int my_pwd();


//for link, unlink, symlink
 void my_link(char *path);
 void my_unlink(char *path);
 void my_symlink(char *path);

//stat, chmod, touch
void my_stat(char *path);
void touch_file(char path[124]);
void chmod_file(char path[124]);



//LEVEL 2 commands

//for open, close
int open_file(char path[124]);
void my_pfd(char *path);
void my_close(char *path);
void close_file(int fd);

//for read, write
int my_write(int fd, char buf[], int nbytes);
void do_write(char *path);
int read_file(char *path);
int my_read(int fd, char *buf, int nbytes);


//lseek, cp, cat, mv
int my_lseek(char *path);
void my_cat(char *path);
void cp_file(char *path);
void mv_file(char *path);

//to QUIT
int quit();
