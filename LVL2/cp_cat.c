#include "commands.h"

void my_cat(char *path)
{
	int n, i;
	int fd = 0;
	char buf[1024];
	int size = 1024;

	int testing = 0;

	//check for path
	if(!path)
	{
		printf("ERROR: missing path\n");
		return;
	}

	//make sure open mode is read
	strcpy(third, "0");

	//open with 0 for RD
	fd = open_file(path);
	//my_pfd("");

	//printf("fd is %d\n", fd);

	//while read returns a number keep reading
	while((n = my_read(fd, buf, size)))
	{
		//printf("count is %d\n", n);

		//null terminate the buffer
		buf[n] = '\0';

		i = 0;

		//print each char in the buffer, this is to handle \n
		while(buf[i])
		{
			putchar(buf[i]);
			if(buf[i] == '\n')
				putchar('\r');
			i++;
		}
	}

	printf("\n\r");

	//close file
	close_file(fd);

	return;
}

void cp_file(char *path)
{
	int i = 0, j;
	int fdDest = 0;
  int fdSrc = 0;
	char buf[1024];
  char src[1024];
  char dest[1024];

	MINODE *mip;
	INODE* ip;

	//printf("Copying file!\n");

  //Check to make sure there is a sorce and dest defined
  //Strcpy to src and dest so we dont lose them
	if (!strcmp(path, ""))
	{
		printf("No source specified!\n");
		return;
	}
  strcpy(src, path);
	if (!strcmp(third, ""))
	{
		printf("No destination specified!\n");
		return;
	}
  strcpy(dest, third);

  //We call our touch function to see if the destination exists already
  //If the dest doesnt exist, touch calls creat to make it for us
  touch_file(dest);

  //Now we open the src for read
  //make sure open mode is read
  strcpy(third, "0");

  //open with 0 for RD
  fdSrc = open_file(src);

  //Now we open the dest for write
  //make sure open mode is write
  strcpy(third, "1");

  //open with 0 for RD
  fdDest = open_file(dest);
	my_pfd("");

  //This loop reads from the src until there is nothing left to read
  //And writes it into the dest
  while (i = my_read(fdSrc, buf, BLKSIZE))
  {
    my_write(fdDest, buf, i);
  }

  //Close our files
  close_file(fdSrc);
  close_file(fdDest);
  my_pfd("");

  //printf("Done!\n");
  return;
}

void mv_file(char *path)
{

	int i = 0, j, ino = 0;
	int fdDest = 0;
  int fdSrc = 0;
	char buf[1024];
  char src[1024];
  char dest[1024];

	MINODE *mip;
	INODE* ip;

	//printf("Moving file!\n");

  //Check to make sure there is a sorce and dest defined
  //Strcpy to src and dest so we dont lose them
	if (!strcmp(path, ""))
	{
		printf("No source specified!\n");
		return;
	}
  strcpy(src, path);
	if (!strcmp(third, ""))
	{
		printf("No destination specified!\n");
		return;
	}
  strcpy(dest, third);

  //Verify that src exists and get its ino
  //Returns out if the src does not exist
  ino = getino(running->cwd, path);
  if(ino == 0)
  {
    printf("Source does not exist!\n");
    return;
  }
  else{
    //printf("source exists!\n");
  }
  //Call our link function which will link src to dest
  //Ensure third is set as destination
  strcpy(third, dest);
  //printf("\ntime to link src to dest... source = %s, dest = %s\n", src, dest);
  my_link(src);
  //printf("link successful\n");

  //Call unlink on src to unlink it to its old parent
  //This removes src from its parents directory
  //this also reduces its link count by one
  //printf("time to remove link from parent...\n");
  my_unlink(src);

  //printf("Done!\n");
  return;
}
