#include <stdio.h>
#include <stdlib.h>


#include "commands.h"

void menu()
{
	printf("\nSUPPORTED COMMANDS:\n");
	printf("  mkdir\t\trmdir\t\tls\t\tcd\n");
	printf("  cd\t\tpwd\t\tcreat\t\tlink\n");
	printf("  unlink\tsymlink\t\tstat\t\tchmod\n");
	printf("  touch\t\tquit\t\tmenu\n\n");
}

int main()
{
	char *commands[24] = {"mkdir", "rmdir", "ls", "cd", "pwd", "creat", "rm", "link", "unlink", "symlink", "stat", "chmod", "touch","open","close","write","read", "pfd","lseek","cat", "cp","mv", "quit", "menu"};
	char device_name[64], input[128] = "";

	char command[64], pathname[64] = "";

	int (*functions[23])(char path[]);
	int i;

//LEVEL ONE FUNCTIONS
	functions[0] = make_directory;
	functions[1] = remove_directory;
	functions[2] = ls;
	functions[3] = cd;
	functions[4] = my_pwd;
	functions[5] = create_file;
	functions[6] = my_unlink;
	functions[7] = my_link;
	functions[8] = my_unlink;
	functions[9] = my_symlink;
	functions[10] = my_stat;
	functions[11] = chmod_file;
	functions[12] = touch_file;
	functions[13] = open_file;
	functions[14] = my_close;
	functions[15] = do_write;
	functions[16] = read_file;
	functions[17] = my_pfd;
	functions[18] = my_lseek;
	functions[19] = my_cat;
	functions[20] = cp_file;
	functions[21] = mv_file;
	functions[22] = quit;
	functions[23] = menu;



	printf("input device name: ");
	scanf("%s", device_name);

	//mount device
	init();
	mount_root(device_name);
	printf("\n\n****************************\n");
	printf("         EXT2 FS            \n");
	printf("    ENTER 'menu' for help     \n");
	printf("****************************\n\n");
	getchar();
	while(1)
	{

		printf("kbsh:~$ ");
		fgets(input, 128, stdin);

		//remove \r at end of input
		input[strlen(input) - 1] = 0;
		if(input[0] == 0)
			continue;

		//split up the input into the variables
		sscanf(input, "%s %s %s", command, pathname, third);
		printf("\n");

		//printf("command: %s\npathname: %s\n", command, pathname);
		//printf("searching for command %s\n", command);

		for(i = 0; i < 23; i++)
		{

			if(!strcmp(commands[i], command))
			{

				(*functions[i])(pathname);
				break;
			}
		}

		if(i == 23)
			printf("invalid command\n");

		//reset variables
		strcpy(pathname, "");
		strcpy(command, "");
		strcpy(input, "");
		strcpy(third, "");
	}
	return 0;
}
