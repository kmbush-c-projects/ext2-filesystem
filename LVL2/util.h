#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <time.h>
#include "type.h"


//GLOBALS
MINODE minode[NMINODE];
MINODE *root;
PROC   proc[NPROC], *running;
MOUNT  mounttab[5];

char cwd[128];
char names[64][128],*name[64];
char third [64];
OFT OpenFileTable[NOFT];
int fd, dev, n;
int nblocks, ninodes, bmap, imap, inode_start, iblock;
int inodeBeginBlock;
char pathname[256], parameter[256];
int DEBUG;

//INITIALIZE DATA STRCTURES AND MOUNT ROOT
//ALSO THIS IS THE PART FOR LAB 7
void init();
int mount_root(char * device);

//utility functions
void get_block(int fd, int blk, char buf[BLKSIZE]);
void put_block(int fd, int blk, char buf[BLKSIZE]);
MINODE*iget(int dev, int ino);
void tokenize(char pathname[256]);
int getino(MINODE* mip, char pathname[64]);
int search(MINODE* mip, char*name);
void iput(MINODE* mip);
int find_name(MINODE*parent, int myino, char*myname);
int findino(MINODE* mip, int* myino, int* parentino);

//alloc/dealloc functions
void set_bit(char *buf, int bit);
void decFreeInodes(int dev);
int tst_bit(char *buf, int bit);
int ialloc(int dev);
int balloc(int dev);
int idealloc(int dev, int ino);
int bdealloc(int dev, int bno);
