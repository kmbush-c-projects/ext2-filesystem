#include "util.h"

void init()
{

  int i;

  //allocate memory for running process
  running = malloc(sizeof(PROC));

  proc[0].pid = 1;
  proc[0].uid = 0;
  proc[0].cwd = 0;

  proc[1].pid = 2;
  proc[1].uid = 1;
  proc[1].cwd = 0;

  running = &proc[0];

  //MINODE minode[100] all with refCount=0
  for(i = 0; i < 100; i++)
  {
    minode[i].refCount = 0;
  }

  //MINODE *root = 0;
  root = 0;

}

//MOUNT THE ROOT OF THE FILE SYSTEM. E.G. CREATE / AND CWD
int mount_root(char* device)
{
  char buf[1024];
  //open device for RW
  dev = open(device, O_RDWR);

  //check if open() worked
  if(dev < 0)
  {
    printf("ERROR: failed cannot open %s\n", device);
    exit(0);
  }

  //read super block to verify it's an EXT2 FS
  get_block(dev, SUPERBLOCK, buf);
  sp = (SUPER *)buf;
  //verify if it's an EXT2 FS
  if(sp->s_magic != 0xEF53)
  {
    printf("NOT AN EXT2 FS\n");
    exit(1);
  }

  //set some vars
  ninodes = sp->s_inodes_count;
  nblocks = sp->s_blocks_count;

  //read group block for info
  get_block(dev, GDBLOCK, buf);
  gp = (GD *)buf;

  imap = gp->bg_inode_bitmap;
  bmap = gp->bg_block_bitmap;

  inodeBeginBlock = gp->bg_inode_table;

  //get root inode
  root = iget(dev, 2);

  //let cwd of both p0 and p1 point at the root minode (refCount=3)
  proc[0].cwd = root;
  proc[1].cwd = root;

  root->refCount = 3;

  printf("%s mounted\n", device);
}

//get block reads a block into a buffer
void get_block(int fd, int blk, char buf[BLKSIZE])
{
  lseek(fd, (long)(blk*BLKSIZE),0);
  read(fd,buf,BLKSIZE);
}

//writes a block back
void put_block(int fd, int blk, char buf[BLKSIZE])
{
  lseek(fd, (long)(blk*BLKSIZE),0);
  write(fd, buf, BLKSIZE);
}

//loads inode into slot in minode array
MINODE*iget(int dev, int ino)
{
  int ipos = 0;
  int i = 0;
  int offset = 0;
  char buf[1024];
  INODE *ip = NULL;
  MINODE *mip = malloc(sizeof(MINODE));

  //First we check to see if we have a minode for the given ino already in use
  //If we do we will just increment the refCount and use that minode
  for (i = 0; i < NMINODE; i++)
  {
    mip = &minode[i];

    if(mip->dev == dev && mip->ino == ino)
    {
      mip->refCount++;
      //printf("minode[%d]->refCount incremented\n", i);
      return mip;
    }
  }

  //mailman's
  ipos = (ino - 1)/8 + INODE_START_POS;
  offset = (ino - 1) % 8;

  //get the block
  get_block(dev, ipos, buf);
  //load inode
  ip = (INODE *)buf + offset;

  for (i = 0; i < NMINODE; i++)
  {
    mip = &minode[i];
    //printf("minode[%d].refCount = %d\n", i, minode[i].refCount);

    if (mip->refCount == 0)
    {
      //printf("using minode[%d]\n", i);
      mip->INODE = *ip;
      mip->dev = dev;
      mip->ino = ino;
      mip->refCount++;

      return mip;
    }
  }
}

//tokenize pathname
void tokenize(char pathname[256])
{
  char path_copy[256] = "";
  char *temp;
  int i = 0;

  for(i = 0; i < 64; i++)
  {
    strcpy(names[i], "");
  }

  i = 0;

  //copy pathname so we don't destroy the original pathname
  strcpy(path_copy, pathname);
  strcat(path_copy, "/");
  temp = strtok(path_copy, "/");

  //populate names
  while(temp != NULL)
  {
    strcpy(names[i], temp);
    temp = strtok(NULL, "/");
    i++;
  }
}

//getts ino for a pathname
int getino(MINODE* mip, char pathname[64])
{
  int ino = 0, i = 0, n = 0;
  int inumber, offset;
  char path[64];
  char name[64][64];
  char *temp;
  char buf[1024];

  //check if it is root
  if(!strcmp(pathname, "/"))
    return 2;//return root ino

  //check if starts at root
  if(pathname[0] == '/')
  {
    mip = root;
  }

  //if there's a pathname, then parse it
  if(pathname)
  {
    //parse the string and put it into name
    strcat(pathname, "/");
    temp = strtok(pathname, "/");

    while(temp != NULL)
    {
      strcpy(name[i], temp);
      temp = strtok(NULL, "/");
      i++;
      n++;
    }
    //parsing complete
  }

  //time to do the searching
  for(i = 0; i < n; i++)
  {
    //printf("inode we are in = %d\n", mip->ino);
    //printf("now searching for %s\n", name[i]);
    ino = search(mip, name[i]);

    if(ino == 0)
    {
      //can't find name[i]
      return 0;
    }

    //printf("found %s at inode %d\n", name[i], ino);

    mip = iget(dev, ino);
  }

  return ino;
}

//searches a minodes datablocks for a name
int search(MINODE* mip, char*name)
{
  //search for name in the data blocks of this INODE
  //if found, return name's ino;
  //return 0

  int i;
  char buf[BLKSIZE], *cp;
  char dir_name[64];
  DIR *dp;

  //make sure it's a directory before we do anything
  if(!S_ISDIR(mip->INODE.i_mode))
  {
    printf("ERROR: not a directory\n");
    return 0;
  }

  //search through the direct blocks
  for(i = 0; i < 12; i++)
  {
    //if the data block has stuff in it
    if(mip->INODE.i_block[i])
    {
      //get the block
      get_block(dev, mip->INODE.i_block[i], buf);

      dp = (DIR *)buf;
      cp = buf;

      while(cp < buf + BLKSIZE)
      {
        //null terminate dp->name for strcmp
        if(dp->name_len < 64)
        {
          strncpy(dir_name, dp->name, dp->name_len);
          dir_name[dp->name_len] = 0;
        }
        else
        {
          strncpy(dir_name, dp->name, 64);
          dir_name[63] = 0;
        }

        //printf("current dir: %s\n", dir_name);
        //check if it's the name we're looking for
        if(strcmp(name, dir_name) == 0)
          return dp->inode;//name matches, return the inode
        cp += dp->rec_len;
        dp = (DIR *)cp;
      }
    }
  }
  //name does not exist, print error message
  printf("name %s does not exist\n", name);
  return 0;
}


void iput(MINODE* mip)
{
  int ino = 0;
  int offset, ipos;
  char buf[1024];

  ino = mip->ino;

  //decrement refCount by 1
  mip->refCount--;

  //check refcount to see if anyone using it
  //check dirty to see if it's been changed, dirty == 1 if changed
  //if refCount > 0 or dirty return
  if (mip->refCount == 0 && mip->dirty == 0)
  {
    return;
  }

  //mail man's to determine disk block and which inode in that block
  ipos = (ino - 1) / 8 + INODE_START_POS;
  offset = (ino -1) % 8;

  //read that block in
  get_block(mip->dev, ipos, buf);

  //copy minode's inode into the inode area in that block
  ip = (INODE*)buf + offset;
  *ip = mip->INODE;

  //write block back to disk
  put_block(mip->dev, ipos, buf);
  mip->dirty = 0;
}


int find_name(MINODE*parent, int myino, char*myname)
{
  int i;
	INODE *ip;
	char buf[BLKSIZE];
	char *cp;
	DIR *dp;

	if(myino == root->ino)
	{
		strcpy(myname, "/");
		return 0;
	}

	if(!parent)
	{
		printf("ERROR: no parent\n");
		return 1;
	}

	ip = &parent->INODE;

	if(!S_ISDIR(ip->i_mode))
	{
		printf("ERROR: not directory\n");
		return 1;
	}

	for(i = 0; i < 12; i++)
	{
		if(ip->i_block[i])
		{
			get_block(dev, ip->i_block[i], buf);
			dp = (DIR*)buf;
			cp = buf;

			while(cp < buf + BLKSIZE)
			{
				if(dp->inode == myino)
				{
					strncpy(myname, dp->name, dp->name_len);
					myname[dp->name_len] = 0;
					return 0;
				}
				else
				{
					cp += dp->rec_len;
					dp = (DIR*)cp;
				}
			}
		}
	}
  return 1;//return 1 if failure
}
int findino(MINODE* mip, int* myino, int* parentino)
{
  INODE *ip;
  char buf[1024];
  char *cp;
  DIR *dp;

  //check if exists
  if(!mip)
  {
    printf("ERROR: ino does not exist!\n");
    return 1;
  }

  //point ip to minode's inode
  ip = &mip->INODE;

  //check if directory
  if(!S_ISDIR(ip->i_mode))
  {
    printf("ERROR: ino not a directory\n");
    return 1;
  }

  //get first block
  get_block(dev, ip->i_block[0], buf);
  dp = (DIR*)buf;
  cp = buf;

  //.
  *myino = dp->inode;

  cp += dp->rec_len;
  dp = (DIR*)cp;

  //..
  *parentino = dp->inode;

  return 0;
}


//sets bit
void set_bit(char *buf, int bit)
{
	int i, j;
	i = bit / 8;
	j = bit % 8;
	buf[i] |= (1 << j);
}

//decrements the amount of free inodes on the device
//this is used to ensure that we don't use more inodes than we
//have room for
void decFreeInodes(int dev)
{
	char buf[BLKSIZE];

	//dec free inodes count in SUPER and Group Descriptor
	get_block(dev, 1, buf);
	sp = (SUPER *)buf;
	sp->s_free_inodes_count--;
	put_block(dev, 1, buf);

	get_block(dev, 2, buf);
	gp = (GD *)buf;
	gp->bg_free_inodes_count--;
	put_block(dev, 2, buf);
}

//tests bit
int tst_bit(char *buf, int bit)
{
	int i, j;
	i = bit / 8;
	j = bit % 8;

	if(buf[i] & (1 << j))
		return 1;

	return 0;
}

//end of helper functions

//allocates a free inode for writing and stuff
//taken from lab 6 pre lab
int ialloc(int dev)
{
	int i;
	char buf[BLKSIZE];
	printf("imap is %d\n", imap);
	//read inode_bitmap block
	get_block(dev, imap, buf);

	printf("inode count %d\n", ninodes);
	//set the bits
	for(i = 0; i < ninodes; i++)
	{
		if(tst_bit(buf, i) == 0)
		{
			//set the bit
			set_bit(buf, i);

			//decrement free inodes on the device
			decFreeInodes(dev);

			//write back to block
			put_block(dev, imap, buf);

			return i + 1;
		}
	}
	printf("ERROR: no more free inodes\n");
	return 0;
}

//allocates a free block so we can put stuff in it
int balloc(int dev)
{
	int i;
	char buf[BLKSIZE];
	printf("bmap is %d\n", bmap);
	//read block_map block
	get_block(dev, bmap, buf);

	//set bits
	for(i = 0; i < nblocks; i++)
	{
		if(tst_bit(buf, i) == 0)
		{
			set_bit(buf, i);

			decFreeInodes(dev);

			put_block(dev, bmap, buf);

			return i;
		}
	}
	printf("ERROR: no more free blocks\n");
	return 0;
}

//WRITE C CODE FOR

//deallocate an inode for a given ino on the dev
int idealloc(int dev, int ino)
{
	char buf[1024];
	int byte;
	int bit;

	//clear bit(bmap, bno)
	get_block(dev, imap, buf);

	//mailmans to where it is
	byte = ino / 8;
	bit = ino % 8;

	//negate it
	buf[byte] &= ~(1 << bit);

	//write back to block
	put_block(dev, imap, buf);

	//increment free blocks
	get_block(dev, 1, buf);
	sp = (SUPER *)buf;
	sp->s_free_blocks_count++;
	put_block(dev, 1, buf);

	get_block(dev, 2, buf);
	gp = (GD *)buf;
	gp->bg_free_blocks_count++;
	put_block(dev, 2, buf);
}

//deallocate a block with bno on the dev
int bdealloc(int dev, int bno)
{
	char buf[1024];
	int byte;
	int bit;

	//clear bit(bmap, bno)
	get_block(dev, bmap, buf);

	//mailman to location
	byte = bno / 8;
	bit = bno % 8;

	//negate it
	buf[byte] &= ~(1 << bit);

	//write back
	put_block(dev, bmap, buf);

	//increment free blocks
	get_block(dev, 1, buf);
	sp = (SUPER *)buf;
	sp->s_free_blocks_count++;
	put_block(dev, 1, buf);

	get_block(dev, 2, buf);
	gp = (GD *)buf;
	gp->bg_free_blocks_count++;
	put_block(dev, 2, buf);

	return 0;
}
