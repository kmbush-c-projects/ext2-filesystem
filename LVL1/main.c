#include <stdio.h>
#include <stdlib.h>


#include "commands.h"

void menu()
{
	printf("COMMANDS:\n");
	printf("  mkdir\t\trmdir\t\tls\t\tcd\n");
	printf("  cd\t\tpwd\t\tcreat\t\tquit\n");
	printf("  menu\t\n");

}

int main()
{
	char *commands[8] = {"mkdir", "rmdir", "ls", "cd", "pwd", "creat",  "quit", "menu"};
	char device_name[64], input[128] = "";

	char command[64], pathname[64] = "";

	int (*functions[23])(char path[]);
	int i;

	functions[0] = make_directory;
	functions[1] = remove_directory;
	functions[2] = ls;
	functions[3] = cd;

	functions[4] = my_pwd;
	functions[5] = create_file;
	functions[6] = quit;
	functions[7] = menu;



	printf("input device name: ");
	scanf("%s", device_name);

	//mount device
	init();
	mount_root(device_name);
	menu();
	getchar();
	while(1)
	{
		printf(">");
		fgets(input, 128, stdin);

		//remove \r at end of input
		input[strlen(input) - 1] = 0;
		if(input[0] == 0)
			continue;

		//split up the input into the variables
		sscanf(input, "%s %s %s", command, pathname, third);

		//printf("command: %s\npathname: %s\n", command, pathname);
		//printf("searching for command %s\n", command);

		for(i = 0; i < 23; i++)
		{
			if(!strcmp(commands[i], command))
			{
				(*functions[i])(pathname);
				break;
			}
		}

		if(i == 23)
			printf("invalid command\n");

		//reset variables
		strcpy(pathname, "");
		strcpy(command, "");
		strcpy(input, "");
		strcpy(third, "");
	}
	return 0;
}
