#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <time.h>

#include "util.h"

//will search an array of commands and return the correct index.
int getCmdIndex(char * cmd);

//for mkdir
void make_directory(char path[124]);
int enter_name(MINODE* mip, int myino, char* myname);
int my_mkdir(MINODE* pmip, char* child_name);

//for rmdir
int isEmpty(MINODE* mip);
void rmChild(MINODE* parent, char* name);
void remove_directory(char* path);

//for creat
void create_file(char path[124]);
int my_creat(MINODE* pmip, char* child_name);

//for rm

//for cd, ls, pwd
void print(MINODE* mip, char* name);
void print_directory(MINODE* mip);

void ls(char* pathname);
void cd(char* pathname);
int my_pwd();


//to QUIT
int quit();
