#include "commands.h"


void print(MINODE *mip, char *name)
{
	int i;
	INODE *ip = &mip->INODE;

	char *permissions = "rwxrwxrwx";

	//information for each file
	u16 mode   = ip->i_mode;
  u16 links  = ip->i_links_count;
  u16 uid    = ip->i_uid;
  u16 gid    = ip->i_gid;
  u32 size   = ip->i_size;


	char *time = ctime((time_t*)&ip->i_mtime);

	//remove \r from time
	time[strlen(time) - 1] = 0;

	switch(mode & 0xF000)
	{
		case 0x8000:  putchar('-');     break; // 0x8 = 1000
    case 0x4000:  putchar('d');     break; // 0x4 = 0100
    case 0xA000:  putchar('l');     break; // oxA = 1010
    default:      putchar('?');     break;
	}

	//print the permissions
	for(i = 0; i < strlen(permissions); i++)
		putchar(mode & (1 << (strlen(permissions) - 1 - i)) ? permissions[i] : '-');

	//print the rest
	printf("%4hu %4hu %4hu %8u %26s  %s\n", links, gid, uid, size, time, name);

	//done
	return;
}

void print_directory(MINODE *mip)
{
	int i;
	DIR *dp;
	char *cp;
	char buf[1024], temp[1024];
	INODE *ip = &mip->INODE;
	MINODE *temp_mip;


	for(i = 0; i < ip->i_size/1024; i++)
	{
		if(ip->i_block[i] == 0)
			break;
		get_block(dev, ip->i_block[i], buf);

		dp = (DIR*)buf;
		cp = buf;

		//goes through the directory and prints the info for every dp
		while(cp < buf + BLKSIZE)
		{
			strncpy(temp, dp->name, dp->name_len);
			temp[dp->name_len] = 0;

			//need to print file permissions and such
			//printf("%s\n", temp);

			temp_mip = iget(dev, dp->inode);
			if(temp_mip)
			{
				//print info for the dp
				print(temp_mip, temp);

				iput(temp_mip);
			}
			else
				printf("ERROR: Cannot print info for Minode\n");

			memset(temp, 0, 1024);
			cp += dp->rec_len;
			dp = (DIR*)cp;
		}
	}
	printf("\n");
}

void ls(char *pathname)
{
	int ino, offset;
	MINODE *mip = running->cwd;
	char name[64][64], temp[64];
	char buf[1024];

	//ls cwd
	if(!strcmp(pathname, ""))
	{
		//print_directory(mip->INODE);
		print_directory(mip);
		return;
	}

	//ls root dir
	if(!strcmp(pathname, "/"))
	{
		//print_directory(root->INODE);
		print_directory(root);
		return;
	}

	//if there's a pathname, ls pathname
	if(pathname)
	{
		//check if path starts at root
		if(pathname[0] == '/')
		{
			mip = root;
		}

		//search for path to print
		ino = getino(mip, pathname);
		if(ino == 0)
		{
			return;
		}

		mip = iget(dev, ino);
		if(!S_ISDIR(mip->INODE.i_mode))
		{
			printf("%s is not a directory!\n", pathname);
			iput(mip);
			return;
		}

		//print_directory(mip->INODE);
		print_directory(mip);
		iput(mip);
	}
	else
	{
		//print root dir
		//print_directory(root->INODE);
		print_directory(root);
	}
}

//change cwd to pathname. If no pathname, set cwd to root.
void cd(char *pathname)
{
	int ino = 0;

	MINODE *mip = running->cwd;
	MINODE *newmip = NULL;

	if (!strcmp(pathname, "")) //If pathname is empty CD to root
	{
		running->cwd = root;
		return;
	}

	if (!strcmp(pathname, "/"))
	{
		running->cwd = root;
		return;
	}

	printf("Path = %s\n", pathname);
	ino = getino(mip, pathname);
	if(ino == 0)
	{
		printf("The directory %s does not exist!\n", pathname);
		return;
	}

	newmip = iget(dev, ino);
	if(!S_ISDIR(newmip->INODE.i_mode))
	{
		printf("%s is not a directory!\n", pathname);
		iput(newmip);
		return;
	}
	//set cwd
	running->cwd = newmip;
	iput(newmip);
	return;
}

//iput all DIRTY minodes before shutdown
int quit()
{
	int i;

	//go through all the minodes
	for(i = 0; i < NMINODE; i++)
	{
		//check if used and dirty
		if(minode[i].refCount > 0 && minode[i].dirty == 1)
		{
			minode[i].refCount = 1;
			iput(&minode[i]);
		}
	}

	printf("Goodbye!\n");
	exit(0);
}

// print the pathname of current working directory
int my_pwd()
{
    // Allocate memory for the path of the cwd
    char path[128] = "";
  	char temp[128] = "";
  	char name[64] = "";
  	int ino, parent_ino;
  	MINODE *mip = running->cwd;

  	//check if at root
  	if(mip == root)
  	{
  		printf("/\n");
  		return;
  	}

  	//keep doing until reaches root
    while(mip != root)
  	{
  		//find ino of cwd and parent
  		findino(mip, &ino, &parent_ino);
  		//set mip to parent
  		mip = iget(dev, parent_ino);
  		//find name using parent and ino
  		find_name(mip, ino, name);

  		//copy so we can have it in the right order
  		strcpy(temp, path);
  		strcpy(path, "");
  		//add to path
  		strcat(path, "/");
  		strcat(path, name);
  		strcat(path, temp);
  	}
  	//print the cwd
  	printf("%s\n", path);

    return 1;
}
