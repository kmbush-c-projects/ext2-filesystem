# EXT2-FileSystem
Part of a course project for CptS 360 at WSU in Spring 2018.

**This project is broken down into two parts:**

Level 1: basic mkdir, rmdir, and such commands

Level 2: has functions such as open, close, read, write, lseek, etc.

**To run this project:**
1. Download source code
2. run a.out (I did not create a make file, you may have to chmod the executable)
3. when prompted, type in the name of the included disk image. It is EXT2 Formatted
4. Program will run. Available commands will be listed on the screen.

**Disclaimer**
I do not guarantee the code will all work perfectly as intended with newer or updated systems. Also, if you wish to reference my work, feel free but please include me as a reference and let me know. 
